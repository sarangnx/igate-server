const http = require('http');
const socketio = require('socket.io');
const fs = require('fs');
const rtmp = require('./rtmp-server').nms;
const zeromq = require('zeromq');

// Http server
let server = http.createServer();

// socket.io
let io = socketio(server);

/**
 * Create a subscriber 'subscriber'
 * For Data Stream
 */
const subscriber = zeromq.socket('sub');

/**
 * Create a consumer (pull) 
 * For Control Stream
 */
const control = zeromq.socket('pull');

/**
 * subscribe to sensor-data
 */
subscriber.subscribe('image');

/**
 * Start RTMP server.
 */
rtmp.run();

io.on('connection', function (socket) {
    console.log('connected');
    
    // socket.emit('crack','test');    
    socket.on('disconnect', function () {
      console.log('disconnected');
    });

    socket.on('start', () => {
        console.log("\n\u001B[1;31mROVER STARTED\u001B[0;0m\n")
    });

    socket.on('stop', () => {
        console.log("\n\u001B[1;31mROVER STOPPED\u001B[0;0m\n")
    });

    socket.on('verified',() => {
        console.log("\n\u001B[1;32mVERIFIED\u001B[0;0m\n")
    });

    socket.on('dismissed',() => {
        console.log("\n\u001B[1;32mDISMISSED\u001B[0;0m\n")
    });
    
    
    // setInterval( ()=>{
    //     socket.emit('crack-detected',{
    //         image: '',
    //         sensor: {
    //             usonic: "10",
    //             ir: "20"
    //         }
    //     })
    // } ,10000);

    socket.on('start-rtmp',()=>{
        if(published){
            socket.emit('rtmp-published',{ port:1935, stream:'/live/stream' })
        }
    });
});

/**
 * Handle Incoming Data Stream.
 * Send sensor-data through socket.io connection
 */
subscriber.on('message',(data) => {
    // io.emit('sensor-data',data.toString());    
    console.log(data.toString('base64'))
    // io.emit('crack-detected',{
    //     image: data.toString('base64'),
    //     sensor: {
    //             usonic: "10",
    //             ir: "20"
    //         }
    // });
});

/**
 * Handle Control Instructions
 */
control.on('message',(command) => {

});
// start http server
server.listen(8000,()=>{
    console.log('server up');
});

rtmp.on('postPublish', (id, StreamPath, args) => {
    setTimeout(()=>{
        io.emit("crack",'test');
    },10000);
    
});

rtmp.on('donePublish',() => {
    // Code to ne run after publishing is finished
});

subscriber.connect('tcp://localhost:7145');
control.connect('tcp://localhost:7146');
