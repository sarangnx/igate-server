/**
 * Unix Sokcet Server used to connect to Python Process.
 * Used for exchanging a single Image frame, on Crack Detection.
 * 
 * Requires the Python process to run first and create a unix socket.
 */
const fs = require('fs');
const net = require('net');

let socket;

/**
 * Used to create a unix socket
 * 
 * @param callback a function with two args (err,socket)
 * 
 * Check if socket is already created.
 * If yes create a connection 
 */
function createSocket(callback){
    fs.access('/tmp/igate.sock', fs.constants.F_OK, (err) => {
        if (err){
            callback(err);
            return;
        }
        try{
            socket = net.createConnection('/tmp/igate.sock',(err)=>{
                if(err){
                    callback(err);
                    return;
                }
            }); 
            callback(null,socket);
        }catch(e){
            callback(e);
        }
    });
}

function registerEventListeners(socket){
    socket.on('connect',()=>{
        console.log('connected');
    });

    /**
    * recieve a single frame of crack, 
    * data is recieved as parts.
    * concatenate it to a buffer.
    */
    frame = Buffer.from('');
    socket.on('data',(data)=>{
        frame = frame + data;
    });     


    /**
    * After completely receiving data,
    * save it to a file.
    */

    socket.on('end',()=>{
        if(frame){
            fs.writeFile('image.jpg',frame.toString('binary'),{encoding:'base64'},(err)=>{
                if(err)
                    console.log(err);
            });
        }
        console.log('connection closed');
    });
    
}
module.exports.createSocket = createSocket;
module.exports.registerEventListeners = registerEventListeners;