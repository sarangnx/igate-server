# Requirements

 - [ZeroMQ](http://zeromq.org/)

```bash
echo "deb http://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/ ./" | sudo tee -a /etc/apt/sources.list
wget https://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/Release.key -O- | sudo apt-key add

sudo apt-get install libzmq3-dev
```

 - FFmpeg
 
 ```bash
 sudo apt install ffmpeg
 ```


## ZEROMQ

Use **PUSH - PULL** for Sending Control Commands.

Use **PUB - SUB** for sending sensor data.

---
# STREAMING

Publish stream to rtmp server using **ffmpeg**.

``` bash
ffmpeg -re -i input.mp4 -c:v h264 -preset superfast -tune zerolatency -an  -ar 44100 -f flv rtmp://localhost/live/stream
```

Publishing through python proecss.
```python
pipe = Popen('ffmpeg -f image2pipe -re -i - -c:v libx264 -preset superfast -tune zerolatency -an -ar 44100 -f flv rtmp://localhost:1935/live/stream', stdin=PIPE,shell=True)
```

`-an` flag disables audio encoding. Which is important as **node-media-server** is having issues with handling the audio while saving the stream to local storage.

## Saving Stream

```bash
ffmpeg -fflags nobuffer -analyzeduration 1000000 -i rtmp://localhost/live/stream -map 0 -c copy -f segment -segment_time 10 -reset_timestamps 1 out%05d.mp4
```

`-f segment -segment_time 10` saves the file as 10 second segments.
