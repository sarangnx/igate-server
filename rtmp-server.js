/**
 * RTMP server setup using npm module NodeMediaServer.
 * Used For streaming Video.
 * 
 * For Publishing Video to RTMP Stream:
 * If you have a video file with H.264 video and AAC audio:
 * $ffmpeg -re -i INPUT_FILE_NAME -c copy -f flv rtmp://localhost/live/STREAM_NAME
 * 
 * Or if you have a video file that is encoded in other audio/video format:
 * $ffmpeg -re -i INPUT_FILE_NAME -c:v libx264 -preset superfast -tune zerolatency -c:a aac -ar 44100 -f flv rtmp://localhost/live/STREAM_NAME
 */
const { NodeMediaServer } = require('node-media-server');

/**
 * node-media-server config
 * logtype  0 = none, 
 *          3 = full
 */
const config = {
    logType: 4,
    rtmp: {
        port: 1935,
        chunk_size: 60000,
        gop_cache: true,
        ping: 60,
        ping_timeout: 30
    }
};

// create and run node-media-server
let nms = new NodeMediaServer(config);

/**
 * StreamStorage Options
 * Defined input rtmp link
 * Path to FFMPEG executable
 * and 
 * Base Output Path
 */
const options = {
    input: 'rtmp://localhost:1935/live/stream',
    ffmpeg: '/usr/bin/ffmpeg',
    outPath: './storage'
}


module.exports = { nms } ;
